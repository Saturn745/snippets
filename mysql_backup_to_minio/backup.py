import os
import sys
import subprocess
from datetime import datetime
from minio import Minio

def main(database_name, database_user, minio_endpoint, minio_access_key, minio_secret_key, minio_bucket_name):
    # Define the temporary file name
    temp_file_name = f"{database_name}-{datetime.now().strftime('%Y-%m-%d-%H-%M-%S')}.sql"    
    # Use mysqldump to dump the database structure and contents to a temporary file
    # Assuming the MySQL server is configured to use a Unix socket for connections
    mysqldump_command = [
        "mysqldump",
        "--socket=/var/lib/mysql/mysql.sock", # Adjust the socket path as necessary
        "--user", database_user,
        "--databases", database_name,
        "--result-file", f"/tmp/{temp_file_name}"
    ]
    subprocess.run(mysqldump_command, check=True)
    
    # Initialize MinIO client
    minio_client = Minio(
        minio_endpoint,
        access_key=minio_access_key,
        secret_key=minio_secret_key,
        secure=True # Set to True if using HTTPS
    )
    
    # Upload the temporary file to the MinIO bucket
    minio_client.fput_object(minio_bucket_name, temp_file_name, f"/tmp/{temp_file_name}")
    
    # Clean up the temporary file
    os.remove(f"/tmp/{temp_file_name}")

if __name__ == "__main__":
    if len(sys.argv) != 7:
        print("Usage: python script.py <database_name> <database_user> <minio_endpoint> <minio_access_key> <minio_secret_key> <minio_bucket_name>")
        sys.exit(1)
    
    database_name = sys.argv[1]
    database_user = sys.argv[2]
    minio_endpoint = sys.argv[3]
    minio_access_key = sys.argv[4]
    minio_secret_key = sys.argv[5]
    minio_bucket_name = sys.argv[6]
    
    main(database_name, database_user, minio_endpoint, minio_access_key, minio_secret_key, minio_bucket_name)
